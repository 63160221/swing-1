/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.swing1;

/**
 *
 * @author AdMiN
 */
import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Frame {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1280, 720);
        
        JLabel lblHelloWorld = new JLabel("Hello World!", JLabel.CENTER);
        frame.add(lblHelloWorld);
        lblHelloWorld.setBackground(Color.ORANGE);
       lblHelloWorld.setOpaque(true);
        lblHelloWorld.setFont(new Font("Verdana", Font.PLAIN, 25));
        frame.setVisible(true);
    }

}
